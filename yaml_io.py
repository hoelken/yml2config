import os
import yaml


def read(file_path: str) -> dict:
    if not os.path.isfile(file_path):
        raise RuntimeError('Given file path "{}" is not a file.'.format(file_path))

    with open(file_path, "r") as f:
        return yaml.safe_load(f)


def write(file_path: str, data: dict) -> None:
    with open(file_path, 'w') as f:
        yaml.safe_dump(data, f)