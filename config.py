from __future__ import annotations

from dataclasses import dataclass, field


@dataclass
class SpecificConfig:
    name: str = 'specific'
    shape_2d: list = field(default_factory=lambda: [2048, 2048])

    @staticmethod
    def from_dict(data: dict):
        return SpecificConfig(**data)


@dataclass
class ConfigItem:
    name: str
    radius: float
    distance: float

    @staticmethod
    def from_dict(data: dict):
        return ConfigItem(**data)


@dataclass
class Config:
    """
    Configuration DataItem
    """

    name: str = 'default'
    boolean_flag: bool = True
    numerical_value: float = 0

    #: The specific configuration. A sub configuration namespace
    specific: SpecificConfig = SpecificConfig()

    #: A list of configuration items of the same type.
    items: list = field(default_factory=lambda: [])

    @staticmethod
    def from_dict(data: dict) -> Config:
        """
        Overwrites the parameters with the values from the given dictionary.
        All instance variable names are supported as keywords.
        Instance variables with a default value are optional and if the keyword is not present the default will be kept.

        ### Returns
        the created Config
        """
        sc = data.pop('specific') if 'specific' in data.keys() else None
        items = data.pop('items') if 'items' in data.keys() else None

        c = Config(**data)
        if sc is not None:
            c.specific = SpecificConfig.from_dict(sc)
        if items is not None:
            c.items = [ConfigItem.from_dict(entry) for entry in items]
        return c
