# yml2config

Small showcase on how to parse Config Items from a YML file

run the example with
```
python parser.py 
```

for the example yml provided that should return 
``` 
--- parsed dictionary --
Config(name='Test Configuration', boolean_flag=True, numerical_value='5e-4', specific=SpecificConfig(name='The specific configuration', shape_2d=[1337, 2048]), items=[ConfigItem(name='Planet A', radius=5, distance=1), ConfigItem(name='Planet B', radius=3, distance=2)])
-----
Config name is: Test Configuration
specific.name is: The specific configuration
config items:
        -  Planet A
        -  Planet B
```

A real world example can be found [here](https://hoelken.pages.gwdg.de/spectroflat/doc/spectroflat/base/config.html)