from config import Config
from yaml_io import read


config = Config.from_dict(read("config.yml"))

print("--- parsed dictionary --")
print(repr(config))
print("-----")

print('Config name is:', config.name)
print('specific.name is:', config.specific.name)
print('config items:')
for ci in config.items:
    print('\t- ', ci.name)

